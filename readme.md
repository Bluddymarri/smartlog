# smartlog

winston based logger for large scale projects

## Availabililty

[![npm](https://pushrocks.gitlab.io/assets/repo-button-npm.svg)](https://www.npmjs.com/package/smartlog)
[![git](https://pushrocks.gitlab.io/assets/repo-button-git.svg)](https://GitLab.com/pushrocks/smartlog)
[![git](https://pushrocks.gitlab.io/assets/repo-button-mirror.svg)](https://github.com/pushrocks/smartlog)
[![docs](https://pushrocks.gitlab.io/assets/repo-button-docs.svg)](https://pushrocks.gitlab.io/smartlog/)

## Status for master

[![build status](https://GitLab.com/pushrocks/smartlog/badges/master/build.svg)](https://GitLab.com/pushrocks/smartlog/commits/master)
[![coverage report](https://GitLab.com/pushrocks/smartlog/badges/master/coverage.svg)](https://GitLab.com/pushrocks/smartlog/commits/master)
[![npm downloads per month](https://img.shields.io/npm/dm/smartlog.svg)](https://www.npmjs.com/package/smartlog)
[![Dependency Status](https://david-dm.org/pushrocks/smartlog.svg)](https://david-dm.org/pushrocks/smartlog)
[![bitHound Dependencies](https://www.bithound.io/github/pushrocks/smartlog/badges/dependencies.svg)](https://www.bithound.io/github/pushrocks/smartlog/master/dependencies/npm)
[![bitHound Code](https://www.bithound.io/github/pushrocks/smartlog/badges/code.svg)](https://www.bithound.io/github/pushrocks/smartlog)
[![Known Vulnerabilities](https://snyk.io/test/npm/smartlog/badge.svg)](https://snyk.io/test/npm/smartlog)
[![TypeScript](https://img.shields.io/badge/TypeScript-2.x-blue.svg)](https://nodejs.org/dist/latest-v6.x/docs/api/)
[![node](https://img.shields.io/badge/node->=%206.x.x-blue.svg)](https://nodejs.org/dist/latest-v6.x/docs/api/)
[![JavaScript Style Guide](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com/)

## Usage

Use TypeScript for best in class instellisense.

For further information read the linked docs at the top of this README.

> MIT licensed | **&copy;** [Lossless GmbH](https://lossless.gmbh)
> | By using this npm module you agree to our [privacy policy](https://lossless.gmbH/privacy.html)

[![repo-footer](https://pushrocks.gitlab.io/assets/repo-footer.svg)](https://push.rocks)
